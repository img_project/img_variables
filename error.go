package imgvariables

import "errors"

var (
	ErrUserExists     = errors.New("user already exists")
	ErrUserNotFound   = errors.New("user not found")
	ErrExpiredToken   = errors.New("token expired")
	ErrInvalidToken   = errors.New("invalid token")
	ErrFileNotFound   = errors.New("file not found")
	ErrNotAllowedFile = errors.New("not allowed file type")
)
