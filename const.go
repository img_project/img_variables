package imgvariables

const (
	DefaultBcryptHashCost = 13
	DefaultChunkSize      = 1024 * 1024
)

var (
	AllowedExtensions = []string{".jpg", ".jpeg", ".png"}
	AllowedMimeTypes  = []string{"image/png", "image/jpeg", "image/jpg"}
)
